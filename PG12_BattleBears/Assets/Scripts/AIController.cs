﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class AIController : Unit 
{
	#region Global Variables
		[SerializeField, Tooltip("Cooldown to shoot")]
		private float ShootCD = 1f;

		[SerializeField]
		private float _EnemyDetectionRadius = 5f;

		[SerializeField]
		private Unit _TargetEnemy;

		[SerializeField]
		private LayerMask _UnitsLayerMask;

		private NavMeshAgent _Agent;
		private IEnumerator _CurrentState;
		private Outpost _TargetOutpost = null;

		
		[SerializeField]
		private GameObject[] _deathParticles;
		[SerializeField]
		private float _ExplosionDamage;

    #endregion Global Variables

    #region Lifecycle

	    protected override void UnitAwake()
		{
			_Agent = GetComponent<NavMeshAgent>();
			StopAllCoroutines();
			SetState(State_Idle());//sET MY DEFAULT STATE
		}



		private void Update()
		{
			_anim.SetFloat("VerticalMovement",_Agent.velocity.magnitude);
		}

	#endregion Lifecycle

	#region StateMachine

	private void SetState(IEnumerator newState)
	{
		if(_CurrentState != null)
		{
			StopCoroutine(_CurrentState);
		}

		_CurrentState = newState;
		StartCoroutine(_CurrentState);
	}

	IEnumerator State_Idle()
	{
		while (_TargetOutpost == null)
		{
			LookForOutpost();
			yield return null;
		}
		CheckKamikazeStateTrigger();
		SetState(State_MovingToOutpost());
	}


    IEnumerator State_MovingToOutpost()
    {
		CheckKamikazeStateTrigger();
		_Agent.SetDestination(_TargetOutpost.transform.position);
		while(_Agent.remainingDistance > _Agent.stoppingDistance)
		{
			yield return null;
		}

		SetState(State_CapturingOutpost());
    }

    IEnumerator State_CapturingOutpost()
    {
		CheckKamikazeStateTrigger();
		while(_TargetOutpost.CurrentTeam != TeamNumber || _TargetOutpost.CaptureValue < 1f)
		{
			LookForEnemy();
			yield return null;
		}

		_TargetOutpost = null;
        SetState(State_Idle());
    }

	IEnumerator State_AttackingEnemy()
	{
		CheckKamikazeStateTrigger();
		_Agent.isStopped = true;
		_Agent.ResetPath();
		float shootTimer = 0;
		while(_TargetEnemy != null &&_TargetEnemy.GetHealth() > 0)
		{
			CheckKamikazeStateTrigger();
			shootTimer += Time.deltaTime;
			transform.LookAt(_TargetEnemy.transform);
			transform.eulerAngles = new Vector3(0,transform.eulerAngles.y,0);
			//Shoot f I can
			if(shootTimer >= ShootCD)
			{
				shootTimer = 0;
				ShootLasersFromEyes(_TargetEnemy.transform.position + Vector3.up, _TargetEnemy.transform);
			}


			//Wait for CD


		yield return null;
		}

		SetState(State_Idle());

	}

	IEnumerator State_Dead()
	{
		yield return null;
	}



 
	

    private void LookForOutpost()
    {
		CheckKamikazeStateTrigger();
		int r = Random.Range(0, Outpost.OutpostList.Count);
		Outpost temp = Outpost.OutpostList[r];
		//If not fully captured of not fully captured by the other team then...
		if(temp.CaptureValue < 1f || temp.CurrentTeam != TeamNumber)
		{
			_TargetOutpost = temp;
		}
		else
		{
			_TargetOutpost = null;
		}
    }

	private void LookForEnemy()
	{
		Collider[] aroundMe = Physics.OverlapSphere(transform.position, _EnemyDetectionRadius, _UnitsLayerMask);
		foreach (var col in aroundMe)
		{
			Unit otherUnit = col.GetComponent<Unit>();
			if(otherUnit != null && otherUnit.TeamNumber != TeamNumber && otherUnit.GetHealth() > 0)
			{
				_TargetEnemy = otherUnit;
				CheckKamikazeStateTrigger();
				SetState(State_AttackingEnemy());
				return;
			}
		}
	}

	protected override void Die()
	{
		base.Die();
		SetState(State_Dead());
		_Agent.isStopped = true;
		_Agent.ResetPath();
		_TargetOutpost = null;
		//Destroy(GetComponent<Collider>());
		Destroy(gameObject);
	}

    #endregion StateMachine

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, _EnemyDetectionRadius);
	}

/*======================================================================*/
/*==================== ASSIGNMENT #1 UNITY 2 ==========================*/
/*======================================================================*/
	IEnumerator State_Kamikaze()
	{
		_Agent.isStopped = false;
		_Agent.ResetPath();
		float kamikazeSpeed = 10f;
		_Agent.speed = kamikazeSpeed;
		while (_TargetEnemy != null && _TargetEnemy.GetHealth() > 0)
		{
			_Agent.SetDestination(_TargetEnemy.transform.position);
			while(_Agent.remainingDistance > _Agent.stoppingDistance)
			{
				yield return null;
			}

			Collider[] _unitsInRange = Physics.OverlapSphere(transform.position, _EnemyDetectionRadius, _UnitsLayerMask);
			foreach (var unit in _unitsInRange)
			{
				//unit.gameObject.GetComponent<Unit>().OnHit(_ExplosionDamage);
				Rigidbody rb = unit.gameObject.GetComponent<Rigidbody>();
				rb.AddExplosionForce(5000,transform.position, 500);
			}
			Destroy(gameObject);
			yield return null;
		}
	}

	private void CheckKamikazeStateTrigger()
	{
		if(GetHealth() <= 25)
		{
			SetState(State_Kamikaze());
			return;
		}
	}

	/// <summary>
	/// This function is called when the MonoBehaviour will be destroyed.
	/// </summary>
	void OnDestroy()
	{
			int random = Random.Range(0,_deathParticles.Length);
			GameObject deathParticle = Instantiate(_deathParticles[random], transform.position, transform.rotation);
			Destroy(deathParticle, 3);
	}
}
