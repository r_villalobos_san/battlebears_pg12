﻿using UnityEngine;

public class NetPlayerConroller : PlayerController 
{
	//Disables the call to Unit.Awake
	private void Awake()
	{
		
	}

	private void Start()
	{
		GameObject teddyClone = PhotonNetwork.Instantiate("Teddy", transform.position, transform.rotation,0);
		teddyClone.transform.SetParent(transform);
		teddyClone.name = "Teddy";

		base.Awake();

		teddyClone.GetComponent<PhotonView>().RPC("RPC_ChangeColor", PhotonTargets.AllBufferedViaServer, TeamNumber);
	}

}
