﻿using UnityEngine;

public class PhotonManager : MonoBehaviour 
{

	#region Global Variables

		private const string VERSION = "PG12";

		[SerializeField]
		private string _RoomName = "PG12";

		[SerializeField]
		private NetPlayerConroller _NetPC;

	#endregion Global Variables

	#region Life Cycle

		private void Start()
		{
			PhotonNetwork.sendRate = 60;
			PhotonNetwork.sendRateOnSerialize = 60;
			PhotonNetwork.ConnectUsingSettings(VERSION);
		}

		private void OnGUI()
		{
			GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
		}

	#endregion Life Cycle

	#region Photon Callbacks

		private void OnConnectedToMaster()
		{
			print("OnConnectedToMaster");
			PhotonNetwork.JoinLobby(TypedLobby.Default);
		}

		private void OnJoinedLobby(){
			print("OnJoinedLobby");
			RoomOptions options = new RoomOptions();
			options.isVisible = true;
			options.MaxPlayers = 20;
			PhotonNetwork.JoinOrCreateRoom(_RoomName, options, TypedLobby.Default);

		}

		private void OnJoinedRoom()
		{
			print("OnJoinedRoom");
			_NetPC.enabled = true;

		}

	#endregion Photon Callbacks

}
