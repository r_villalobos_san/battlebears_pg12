﻿using UnityEngine;
using Photon;

public class TransformSyncer : PunBehaviour 
{
	[SerializeField]
	private float _SmoothFactor = 0.01f;

	private Vector3 _NetPos;

	private void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if(stream.isWriting)//Im sending a message
		{
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation);
			//stream.SendNext(transform.lossyScale);
		}	
		else//Im receiving a message
		{
			_NetPos = (Vector3)stream.ReceiveNext();
			transform.rotation = (Quaternion)stream.ReceiveNext();
			//transform.localScale = (Vector3)stream.ReceiveNext();
		}
	}

	private void Update()
	{
		if(photonView.isMine == false)//If this guy isnot mine
		{
			//transform.position = Vector3.Lerp(transform.position, _NetPos , _SmoothFactor);
			transform.position = HermitMe(transform.position, _NetPos , _SmoothFactor);
		}
	}

	[PunRPC]
	public void RPC_ChangeColor(int team)
	{
		Color teamColor = GameManager.Instance.TeamColors[team];
		transform.Find("Teddy_Body").GetComponent<Renderer>().material.color = teamColor;

	}

	private Vector3 HermitMe(Vector3 from, Vector3 to, float smooth)
	{
		return new Vector3(
			Hermite(from.x, to.x, smooth),
			Hermite(from.y, to.y, smooth),
			Hermite(from.z, to.z, smooth)
		);

	}

	private float Hermite(float from, float to, float smooth)
	{
		return Mathf.Lerp(from, to, (3*Mathf.Pow(smooth,2)) - (2*Mathf.Pow(smooth,3)));
	}

}
