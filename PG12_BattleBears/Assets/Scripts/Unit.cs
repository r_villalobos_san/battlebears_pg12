﻿using System;
using UnityEngine;

public abstract class Unit : MonoBehaviour 
{

	public int TeamNumber;
	[SerializeField]
	private float _Health = 100f;

	[SerializeField]
	private float _AttackDamage = 8f;

	protected Animator _anim;
	protected bool _IsAlive = true;

	[SerializeField]
		private Laser _LaserPrefab;
				private Eye[] _Eyes;

	protected void Awake()
	{
		_anim = GetComponentInChildren<Animator>();
		_Eyes = GetComponentsInChildren<Eye>(); 
		if(_Eyes.Length <= 0)
		{
			Debug.Log("ERROR: No eyes component found on Teddys children");
		}
		Color teamColor = GameManager.Instance.TeamColors[TeamNumber];
		transform.Find("Teddy/Teddy_Body").GetComponent<Renderer>().material.color = teamColor;
		UnitAwake();
	}

	protected abstract void UnitAwake();

	public float GetHealth()
	{
		return _Health;
	}

    protected void ShootLasersFromEyes(Vector3 hitPos, Transform other)
    {
        foreach (var Eye in _Eyes)
		{
			Instantiate(_LaserPrefab).Shoot(Eye.transform.position, hitPos);
		}

		Unit otherUnit = other.GetComponent<Unit>();
		if(otherUnit != null && otherUnit.TeamNumber != TeamNumber)
		{
			//deal damage
			otherUnit.OnHit(_AttackDamage);
		}
    }

	protected bool CanSee(Vector3 hitPos, Transform other)
		{

			foreach (var Eye in _Eyes)
			{
				Vector3 startPos = Eye.transform.position;
				Vector3 dir = hitPos - startPos;
				Ray ray = new Ray(startPos, dir);
				RaycastHit hit;
				if(Physics.Raycast(ray, out hit))
				{
					if(hit.transform == other)
					{
						return true;
					}
				}
			}
			
			return false;
		}

	public void OnHit(float damage)
	{
		_Health -= damage;
		if(_Health <= 0)
		{
			_Health = 0;
			Die();
		}
	}

    protected virtual void Die()
    {
		_IsAlive = false;
        _anim.SetBool("IsDead", true);
    }
}
